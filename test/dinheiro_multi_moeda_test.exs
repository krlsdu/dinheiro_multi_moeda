defmodule DinheiroMultiMoedaTest do
  use ExUnit.Case
  doctest DinheiroMultiMoeda

  test "multiplication" do
    assert DinheiroMultiMoeda.amount(2, 5) == 10
  end
end
