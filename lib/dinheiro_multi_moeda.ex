defmodule DinheiroMultiMoeda do
  def amount(times, value) do
    times * value
  end
end
